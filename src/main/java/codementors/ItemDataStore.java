package codementors;

import codementors.model.*;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class ItemDataStore {

    private static final Logger log = Logger.getLogger(ItemDataStore.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Item> sortItems(boolean a, String item) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> i = query.from(Item.class);
        query.select(i);
        if(a) {
            query.orderBy(cb.desc(i.get(item).get("name")));
        }else{
            query.orderBy(cb.asc(i.get(item).get("name")));
        }
        return em.createQuery(query).getResultList();
    }

    public List<Item> sortItemsName(boolean a) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Item> query = cb.createQuery(Item.class);
        Root<Item> item = query.from(Item.class);
        query.select(item);

        if(a) {
            query.orderBy(cb.desc(item.get("name")));
        }else{
            query.orderBy(cb.asc(item.get("name")));
        }
        return em.createQuery(query).getResultList();
    }

    public void deleteItem(Item item) {
        em.remove(em.merge(item));
    }


    public List<Category> getCategory() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> query = cb.createQuery(Category.class);
        query.from(Category.class);
        return em.createQuery(query).getResultList();
    }

    public List<Department> getDepartment() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Department> query = cb.createQuery(Department.class);
        query.from(Department.class);
        return em.createQuery(query).getResultList();
    }

    public List<Warehouse> getWarehouse() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Warehouse> query = cb.createQuery(Warehouse.class);
        query.from(Warehouse.class);
        return em.createQuery(query).getResultList();
    }

    public void deleteWarehouse(Warehouse warehouse) {
        em.remove(em.merge(warehouse));
    }

    public void createItem(Item item) {
        em.persist(item);
    }

    public Category getCategory(int id) {
        return em.find(Category.class, id);
    }

    public Department getDepartment(int id) {
        return em.find(Department.class, id);
    }

    public Warehouse getWarehouse(int id) {
        return em.find(Warehouse.class, id);
    }

    public Tag getTag(int id) {
        return em.find(Tag.class, id);
    }

    public List<Tag> getTag() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        query.from(Tag.class);
        return em.createQuery(query).getResultList();

    }
    public void createCategory(Category category) {
        em.persist(category);
    }

    public void createDepartment(Department department) {
        em.persist(department);
    }

    public void createWarehouse(Warehouse warehouse) {
        em.persist(warehouse);
    }

    public void createTag(Tag tag){
        em.persist(tag);
    }

}
