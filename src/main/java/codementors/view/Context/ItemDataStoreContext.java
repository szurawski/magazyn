package codementors.view.Context;

import codementors.model.*;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.logging.Logger;

@Singleton
@Startup
public class ItemDataStoreContext {

    private static final Logger log = Logger.getLogger(ItemDataStoreContext.class.getName());

    @PersistenceContext
    private EntityManager em;

    @PostConstruct
    public void init() {

        Category category1 = new Category("Wahache");
        Category category2 = new Category("Intrudery");
        Category category3 = new Category("Siekiery");

        em.persist(category1);
        em.persist(category2);
        em.persist(category3);

        Department dep1 = new Department("Ochrona");
        Department dep2 = new Department("Admin");
        Department dep3 = new Department("Szef");

        em.persist(dep1);
        em.persist(dep2);
        em.persist(dep3);

        Warehouse ware1 = new Warehouse("Magazyn 1");
        Warehouse ware2 = new Warehouse("Magazyn 2");
        Warehouse ware3 = new Warehouse("Magazyn 3");

        em.persist(ware1);
        em.persist(ware2);
        em.persist(ware3);

        Tag tag1 = new Tag("fajny");
        Tag tag2 = new Tag("chujowy");
        Tag tag3 = new Tag("może być");

        em.persist(tag1);
        em.persist(tag2);
        em.persist(tag3);


        Item item1 = new Item();
        item1.setName("duap");
        item1.setPath("/tmp");
        item1.setWarehouse(ware1);
        item1.setDepartment(dep1);
        item1.setCategory(category1);
        item1.setTags(Arrays.asList(tag1, tag2));
        em.persist(item1);



    }


}
