package codementors.view.ListView;



import codementors.TagDataStore;
import codementors.model.Tag;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class TagsList implements Serializable {

    @EJB
    private TagDataStore store;


    private List<Tag> tags;

    public List<Tag> getTags() {
        if (tags == null) {
            tags =store.getTags();
        }
        return tags;
    }


    public void delete(Tag tag) {
        store.deleteTag(tag);
        tags = null;
    }
}
