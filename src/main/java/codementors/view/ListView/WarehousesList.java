package codementors.view.ListView;

import codementors.ItemDataStore;
import codementors.model.Warehouse;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class WarehousesList implements Serializable {

    @EJB
    private ItemDataStore store;

    private List<Warehouse> warehouses;

    private Warehouse warehouse;

    public List<Warehouse> getWarehouses() {
        if (warehouses == null) {
            warehouses = store.getWarehouse();
        }
        return warehouses;
    }

    public void delete(Warehouse warehouse) {
        store.deleteWarehouse(warehouse);
        warehouses = null;
    }

}

