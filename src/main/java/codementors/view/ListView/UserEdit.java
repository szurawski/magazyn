package codementors.view.ListView;


import codementors.UserDataStore;
import codementors.model.User;

import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Named
@ViewScoped
public class UserEdit implements Serializable {

    @EJB
    private UserDataStore store;

    private int userId;

    private User user;

    private List<SelectItem> users;

    private List<SelectItem> roles;

    public int getUserId() {
        return userId;
    }

    public UserDataStore getStore() {
        return store;
    }

    public void setStore(UserDataStore store) {
        this.store = store;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<SelectItem> getUsers() {
        return users;
    }

    public void setUsers(List<SelectItem> users) {
        this.users = users;
    }

    public void setRoles(List<SelectItem> roles) {
        this.roles = roles;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        if (user == null) {
            user = store.getUser(userId);
        }
        return user;
    }

    public List<SelectItem> getRoles() {
        if (roles == null) {
            roles = new ArrayList<SelectItem>();
            for (User.Role role : User.Role.values()) {

                roles.add(new SelectItem(role, role.name()));

            }}
            return roles;
        }


        public void saveUser(){
            store.updateUser(user);
        }



}

