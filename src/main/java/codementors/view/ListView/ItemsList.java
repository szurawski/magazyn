package codementors.view.ListView;


import codementors.ItemDataStore;
import codementors.model.Item;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@ViewScoped
public class ItemsList implements Serializable {

    @EJB
    private ItemDataStore store;

   
    private List<Item> items;

    public void delete(Item item) {
        store.deleteItem(item);
        items = null;
    }
    public List<Item> getItems() {
        if (items == null) {
            items = store.sortItems(true, "department");
        }
        return items;
    }

    public void sortItems(boolean b, String item){
        if(b) {
            items = store.sortItems(true, item);
        }else{
            items = store.sortItems(false, item);
        }
    }
    public void sortItemsName(boolean b){

        if(b) {
            items = store.sortItemsName(true);
        }else{
            items= store.sortItemsName(false);
        }

    }
}
