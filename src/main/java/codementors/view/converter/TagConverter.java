package codementors.view.converter;

import codementors.ItemDataStore;
import codementors.model.Tag;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass = Tag.class, value = "tagConverter")
public class TagConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String s) {
        ItemDataStore store = CDI.current().select(ItemDataStore.class).get();
        if (s == null || s.equals("null")) {
            return null;
        }
        return store.getTag().get(Integer.parseInt(s));
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        ItemDataStore store = CDI.current().select(ItemDataStore.class).get();
        if (o == null) {
            return "null";
        }
        return store.getTag().indexOf((Tag)o)+"";
    }
}
