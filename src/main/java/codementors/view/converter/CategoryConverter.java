package codementors.view.converter;

import codementors.ItemDataStore;
import codementors.model.Category;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("categoryConverter")
public class CategoryConverter implements Converter {


    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        ItemDataStore store = CDI.current().select(ItemDataStore.class).get();
        return store.getCategory(Integer.parseInt(value));//Parse id in string to integer.

    }

        @Override
    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object o) {
        return ((Category)o).getId()+"";
    }
}

