package codementors.view.AddingElements;

import codementors.ItemDataStore;
import codementors.model.Warehouse;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class AddWarehouse implements Serializable {

    @EJB
    private ItemDataStore store;

    private Warehouse warehouse = new Warehouse();

    public void saveCategory() {
        store.createWarehouse(warehouse);

    }

    public ItemDataStore getStore() {
        return store;
    }

    public void setStore(ItemDataStore store) {
        this.store = store;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse department) {
        this.warehouse = department;
    }
}


