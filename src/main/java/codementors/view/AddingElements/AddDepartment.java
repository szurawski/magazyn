package codementors.view.AddingElements;

import codementors.ItemDataStore;
import codementors.model.Department;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class AddDepartment implements Serializable {

    @EJB
    private ItemDataStore store;

    private Department department = new Department();

    public void saveCategory() {
        store.createDepartment(department);

    }
    public ItemDataStore getStore() {
        return store;
    }
    public void setStore(ItemDataStore store) {
        this.store = store;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }



}
