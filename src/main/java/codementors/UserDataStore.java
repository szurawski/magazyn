package codementors;

import codementors.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class UserDataStore {

    private static final Logger log = Logger.getLogger(UserDataStore.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<User> getUsers() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        query.from(User.class);
        return em.createQuery(query).getResultList();
    }

    public User getUser(int id) {
        User user = em.find(User.class, id);
        em.detach(user);

        return user;
    }

    public void deleteUser(User user) {
        em.remove(em.merge(user));
    }

    public void updateUser(User user) {
        em.merge(user);
    }

    public List<User.Role> getRoles() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User.Role> query = cb.createQuery(User.Role.class);
        query.from(User.Role.class);
        return em.createQuery(query).getResultList();
    }


    public void createUser(User user) {
        em.persist(user);
    }

}
