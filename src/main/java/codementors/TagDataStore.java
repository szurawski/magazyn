package codementors;

import codementors.model.Tag;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class TagDataStore {
    private static final Logger log = Logger.getLogger(ItemDataStore.class.getName());

    @PersistenceContext
    private EntityManager em;
    public List<Tag> getTags() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
        query.from(Tag.class);
        return em.createQuery(query).getResultList();
    }

    public void deleteTag(Tag tag) {
        em.remove(em.merge(tag));
    }
}
