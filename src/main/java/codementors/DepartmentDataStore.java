package codementors;

import codementors.model.Department;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.logging.Logger;

@Stateless
public class DepartmentDataStore {
    private static final Logger log = Logger.getLogger(ItemDataStore.class.getName());

    @PersistenceContext
    private EntityManager em;

    public List<Department> getDepartments() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Department> query = cb.createQuery(Department.class);
        query.from(Department.class);
        return em.createQuery(query).getResultList();
    }

    public void deleteDepartment(Department department) {
        em.remove(em.merge(department));
    }
}
