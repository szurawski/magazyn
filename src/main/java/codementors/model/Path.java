package codementors.model;

import javax.persistence.*;

@Entity
@Table(name = "path")
public class Path implements java.io.Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer PathId;

    @Column
    private byte[] image;

    public Path() {
    }

    public Integer getPathId() {
        return PathId;
    }

    public void setPathId(Integer pathId) {
        PathId = pathId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}

