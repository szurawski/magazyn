package codementors.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "items")
public class Item  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private String path;

    @Column
    private LocalDateTime date;

    @ManyToOne()
    @JoinColumn(name = "department", referencedColumnName = "id")
    private Department department;


    @ManyToOne()
    @JoinColumn(name = "categories", referencedColumnName = "id")
    private Category category;

    @ManyToOne()
    @JoinColumn(name = "warehouses", referencedColumnName = "id")
    private Warehouse warehouse;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "item_tag", joinColumns = @JoinColumn(name = "items", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tags", referencedColumnName = "name"))
    private List<Tag> tags = new ArrayList<>();


    public Item() {
        this.date = LocalDateTime.now();
    }


    public Item(LocalDateTime localDateTime, String name, String path, Department department, Category category, Warehouse warehouse, List<Tag> tags) {
        this.date = localDateTime;
        this.name = name;
        this.path = path;
        this.department = department;
        this.category = category;
        this.warehouse = warehouse;
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category categories) {
        this.category = categories;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
